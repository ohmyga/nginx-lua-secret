# nginx-lua 防盗链
nginx lua模块 diy防盗链，url加密

#使用方法

1.为nginx安装lua-nginx-module模块 
下载地址：https://github.com/openresty/lua-nginx-module

2.开启防盗链
在需要开启防盗的目录片段（location）中加入

rewrite_by_lua_file "conf/fd.lua";

例如：

location /down {

	default_type "text/html";

	rewrite_by_lua_file "conf/fd.lua";

}

3.测试

需要访问的url 必须带有sign和timestamp参数 否则将返回405状态

sign加密规则为 $sign = md5(uri|时间戳|密钥)
