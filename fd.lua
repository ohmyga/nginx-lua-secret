--lua 防盗链
local secret = 'hehe'	--签名密钥
local expire = 14400	--过期时间
local isJump = 0		--开启签名错误跳转 0:关闭 1:开启
local jumpUrl = 'http://www.lipeng.org'	--跳转地址
local superSign = '123456'	--超级签名(测试用),使用后不验证签名和时间戳
local _uri = ngx.var.uri
local _sign = ngx.var.arg_sign
local _timestamp = ngx.var.arg_timestamp
local sign = ''
local time = ngx.now()
local _error
local _debug

--错误返回
_error = function(msg,code)
	ngx.header.ngxin_error= msg
	if isJump == 1 then
		ngx.redirect(jumpUrl)
	end
	ngx.exit(code)
end;

--debug变量调试
_debug = function()
	ngx.say('uri:'.._uri)
	ngx.say('sign:'.._sign)
	ngx.say('timestamp:'.._timestamp)
	ngx.say('curr_time:'..time)
end
--_debug()
if(_sign ~= superSign) then
	--检测传递的sign
	if _sign == nil then
		_error('sign is null',405)
	end

	--检测传递的timestamp
	if _timestamp == nil then
		_error('timestamp is null',405)
		--ngx.exit(405)
	end
	
	--验证sign
	sign = ngx.md5(_uri..'|'.._timestamp..'|'..secret)
	if sign ~= _sign then
		_error('sign error',405)
	end

	--验证过期时间
	if time > (_timestamp + expire) then
		_error('uri expire',405)
	end
end